﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace CmmsWebService.DB
{
    public class DL52WeekPPM
    {
        public DataSet GetPlannedActivities_Service(int companyid, int locationid, int yearid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetPlannedActivities_Service");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "@Year", DbType.Int32, yearid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetPlannedActivities_Statuswise(int companyid, int locationid, int yearid, int weekid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetStatusDetailbyWeek");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "@Year", DbType.Int32, yearid);
                db.AddInParameter(cmd, "@Week", DbType.Int32, weekid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetUpdatedDetailsByWeek(int companyid, int locationid, int yearid, int weekid, int statusid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetUpdatedDetailsByWeek");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "@Year", DbType.Int32, yearid);
                db.AddInParameter(cmd, "@Week", DbType.Int32, weekid);
                db.AddInParameter(cmd, "@StatusId", DbType.Int32, statusid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetStatusDetails(int companyId, int locationid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
            DbCommand cmd = db.GetStoredProcCommand("SP_Get_StatusDetails");
            db.AddInParameter(cmd, "@StatusID", DbType.Int32, 0);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, companyId);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, locationid);
            db.AddInParameter(cmd, "@Type", DbType.String, "Asset Update");
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetAssignedDetails_Mobile(int assetAssignId)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetAssignedDetails_Mobile");
            db.AddInParameter(cmd, "@AssetAssignId", DbType.Int32, assetAssignId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet GetAssetAssignAndOverDueDetials(int assetid, int year, int weekno)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetAssetAssignAndOverDueDetials");
            db.AddInParameter(cmd, "@AssetId", DbType.Int32, assetid);
            db.AddInParameter(cmd, "@YearId", DbType.Int16, year);
            db.AddInParameter(cmd, "@WeekId", DbType.Int16, weekno);


            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetDepartmentDetails(int companyid,int locationid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiAsstz"); 
            DbCommand cmd = db.GetStoredProcCommand("SP_Get_DepartmentDetails");
            db.AddInParameter(cmd, "@DepartmentID", DbType.Int32,0);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, companyid);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, locationid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string[] AssetUpdate(int assetassignId, int statusId, string remarks, bool isOverdue, int weekdayid, int userid, DateTime? updateddate, string checklistxml, string files)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateAssetMobile");
                db.AddInParameter(cmd, "@AssetAssignId", DbType.Int32, assetassignId);
                db.AddInParameter(cmd, "@StatusId", DbType.Int32, statusId);
                db.AddInParameter(cmd, "@Remarks", DbType.String, remarks);
                db.AddInParameter(cmd, "@IsOverDue", DbType.Boolean, isOverdue);
                db.AddInParameter(cmd, "@WeekDayId", DbType.Int32, weekdayid);
                if (updateddate != null)
                {
                    db.AddInParameter(cmd, "@UpdatedDate", DbType.DateTime, updateddate);
                }
                else
                {
                    db.AddInParameter(cmd, "@UpdatedDate", DbType.DateTime, DBNull.Value);
                }
                db.AddInParameter(cmd, "@ChecklistXML", DbType.String, checklistxml);
                db.AddInParameter(cmd, "@uploadedimages", DbType.String, files);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddInParameter(cmd, "@UID", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@Status", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {

                throw;
            }

        }


    }
}