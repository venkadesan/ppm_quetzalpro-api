﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI.WebControls.Expressions;

namespace CmmsWebService.Class
{
    public class Model
    {
        public class StatusData
        {
            public bool Status { set; get; }
            public string Message { set; get; }
        }

        public class User
        {
            public int UserID { get; set; }
            public int CompanyID { get; set; }
            public int LocationID { get; set; }
            public string CompanyName { get; set; }
            //public string CompanyShortName { get; set; }
            public string LocationName { get; set; }
            //public string LocationShortName { get; set; }
            public string UserFirstName { get; set; }
            public string UserLastName { get; set; }
            public int GroupId { get; set; }

            public string Language { get; set; }
            public int ProductID { get; set; }

            public bool Status { set; get; }
            public string Message { set; get; }
        }

        public class ResponseDataNew
        {
            public object Data { set; get; }
        }

        public class PlannedActivity
        {
            public int Week { get; set; }
            public int Planned { get; set; }
            public string Wdate { get; set; }
            public bool IsCurrentMonth { get; set; }
            public bool IsPending { get; set; }
            public bool IsClosed { get; set; }
            public int Closed { get; set; }
        }

        public class StatusPlannedActivity
        {
            public int StatusId { get; set; }
            public int Count { get; set; }
            public string Status { get; set; }
        }

        public class Company
        {
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
        }

        public class  Location
        {
            public int LocationId { get; set; }
            public string LocationName { get; set; }
        }

        public class UpdatedDetails
        {
            public int AssetId { get; set; }
            public int WeekId { get; set; }
            public int StatusId { get; set; }
            public string AssetName { get; set; }
            public string PlannerId { get; set; }
            public bool IsClosed { get; set; }
            public string Status { get; set; }
            public int DepartmentId { get; set; }

            public int AssetAssignId { get; set; } 
            public string Frequency { get; set; }
            public string BuildingShortName { get; set; }
            public string Building { get; set; }

            public string FloorShortName { get; set; }
            public string Floor { get; set; }
            public string FDate { get; set; }
            public string FTime { get; set; }
        }

        public class Status
        {
            public int StatusId { get; set; }
            public string StatusName { get; set; }
            public bool isclosed { get; set; }
        }

        public class AssignedDet
        {
            public int AssetAssignedId { get; set; }
            public string PlannerName { get; set; }
            public string AssetName { get; set; }
            public string FrequencyDate { get; set; }
            public int StatusId { get; set; }
            public string Status { get; set; }
            public bool IsClosed { get; set; }
            public bool IsOverDue { get; set; }
            public int WeekId { get; set; }
            public string Remarks { get; set; }
            public List<Model.Checklist> checklists { get; set; }
        }

        public class Checklist
        {
            public string checklist { get; set; }
            public int checklistid { get; set; }
            public bool IsChecked { get; set; }
            public int Slno { get; set; }
            public string Remarks { get; set; }
        }

        public class Department
        {
            public int DepartmentId { get; set; }
            public string DepartmentName { get; set; }
        }

        public class BreakdownList
        {
            public int BreakdownId { get; set; }
            public string BreakdownDate { get; set; }
            public string ClosedDate { get; set; }
            public string SupplierDetails { get; set; }
            public string CapaDoc { get; set; }
            public string BreakdownImage { get; set; }
            public string ClosedImage { get; set; }
        }

        public class CheckBreakdown
        {
            public bool IsUnderBreakdown { get; set; }
        }

        public class InsertBreakdown
        {
            public int Assetid { get; set; }
            public string Reason { get; set; }
            public string AddImage { get; set; }
            public DateTime StartDate { get; set; }
            public TimeSpan StartTime { get; set; }
            public int UserId { get; set; }
        }

        public class UpdateBreakDown
        {
            public int BreakdownId { get; set; }
            public string RCA { get; set; }
            public string UpdateImage { get; set; }
            public string CAPADoc { get; set; }
            public DateTime UpdateDate { get; set; }
            public TimeSpan UpdateTime { get; set; }
            public int UserId { get; set; }
            public string SupplierDetails { get; set; }
            public int UserId { get; set; }
        }
    }
}