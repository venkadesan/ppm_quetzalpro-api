﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using CmmsWebService.Class;
using CmmsWebService.DB;
using Newtonsoft.Json;
using MCB = Kowni.Common.BusinessLogic;
using KB = Kowni.BusinessLogic;
using System.IO;
using System.Configuration;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
namespace CmmsWebService
{
    /// <summary>
    /// Summary description for Week52PPMWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Week52PPMWebService : System.Web.Services.WebService
    {
        Kowni.Common.BusinessLogic.BLSingleSignOn objSingleSignOn = new Kowni.Common.BusinessLogic.BLSingleSignOn();
        //BLLocation _objLocation = new BLLocation();
        //BLCompany _objCompany = new BLCompany();

        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();
        KB.BLLanguage objlang = new KB.BLLanguage();

        DL52WeekPPM _objdlweek = new DL52WeekPPM();
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        #region "Check Login Credentials"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetLoginDetails(string username, string password)
        {
            string _res = string.Empty;

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Username or Password", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            try
            {
                int RoleID = 0;
                int UserID = 0;
                int CompanyID = 0;
                string CompanyName = string.Empty;
                int LocationID = 0;
                string LocationName = string.Empty;
                int GroupID = 0;
                int LanguageID = 0;
                string UserFName = string.Empty;
                string UserLName = string.Empty;
                string UserMailID = string.Empty;
                string ThemeFolderPath = string.Empty;

                if (!objSingleSignOn.Login(username, password, 27, out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
                {


                    Model.ResponseDataNew rd = new Model.ResponseDataNew();
                    Model.User lstUser = new Model.User();
                    lstUser.UserID = UserID;
                    lstUser.CompanyID = CompanyID;
                    lstUser.LocationID = LocationID;
                    lstUser.CompanyName = CompanyName;
                    lstUser.LocationName = LocationName;
                    //lstUser.CompanyShortName = string.Empty;
                    //lstUser.LocationShortName = string.Empty;
                    lstUser.UserFirstName = UserFName;
                    lstUser.UserLastName = UserLName;
                    lstUser.GroupId = GroupID;

                    lstUser.Language = string.Empty;
                    lstUser.ProductID = 66;

                    lstUser.Message = "Login Falied";
                    lstUser.Status = false;
                    rd.Data = lstUser;

                    // _res = JsonConvert.SerializeObject(new Model.ResponseDataNew() { Message = "Unauthorized Access. " + objSingleSignOn.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //string result = serializer.Serialize(lstUser);

                    List<object> resultMains = new List<object>();
                    object resultMain = new object();
                    resultMain = lstUser;
                    resultMains.Add(resultMain);
                    string results = serializer.Serialize(resultMains);
                    return results;
                }
                else
                {
                    DataSet dslangdet = new DataSet();
                    dslangdet = objlang.GetLanguages(LanguageID);
                    string language = string.Empty;
                    if (dslangdet != null && dslangdet.Tables.Count > 0 && dslangdet.Tables[0].Rows.Count > 0)
                    {

                        language = dslangdet.Tables[0].Rows[0]["LanguageName"].ToString();
                    }

                    Model.User lstUser = new Model.User();
                    lstUser.UserID = UserID;
                    lstUser.CompanyID = CompanyID;
                    lstUser.LocationID = LocationID;
                    lstUser.CompanyName = CompanyName;
                    lstUser.LocationName = LocationName;
                    //lstUser.CompanyShortName = string.Empty;
                    //lstUser.LocationShortName = string.Empty;
                    lstUser.UserFirstName = UserFName;
                    lstUser.UserLastName = UserLName;
                    lstUser.GroupId = GroupID;

                    lstUser.Language = language;
                    lstUser.ProductID = 66;

                    lstUser.Message = "Login Success";
                    lstUser.Status = true;

                    //DataSet dscompanyDet = new DataSet();
                    //dscompanyDet = _objCompany.GetCompany(CompanyID);
                    //if (dscompanyDet != null && dscompanyDet.Tables.Count > 0 && dscompanyDet.Tables[0].Rows.Count > 0)
                    //{
                    //    DataRow drcompanyDet = dscompanyDet.Tables[0].Rows[0];
                    //    lstUser.CompanyShortName = drcompanyDet["companyShortName"].ToString();
                    //}

                    //DataSet dslocDet = new DataSet();
                    //dslocDet = _objLocation.GetLocation_ByID(LocationID);
                    //if (dslocDet != null && dslocDet.Tables.Count > 0 && dslocDet.Tables[0].Rows.Count > 0)
                    //{
                    //    DataRow drlocDet = dslocDet.Tables[0].Rows[0];
                    //    lstUser.LocationShortName = drlocDet["LocationShortName"].ToString();
                    //}

                    Model.ResponseDataNew rd = new Model.ResponseDataNew();
                    rd.Data = lstUser;

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //string result = serializer.Serialize(lstUser);

                    List<object> resultMains = new List<object>();
                    object resultMain = new object();
                    resultMain = lstUser;
                    resultMains.Add(resultMain);
                    string results = serializer.Serialize(resultMains);
                    return results;
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again. " + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            return _res;
        }

        #endregion

        #region "Get Planned Activities"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetPlannedActivities_Service(string companyid, string locationid, string yearid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(companyid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(locationid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }


                if (string.IsNullOrWhiteSpace(yearid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Year", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                // yearid = "2017";//need to be dynamic, once got confirmation.

                int icompanyid = Convert.ToInt32(companyid);
                int ilocationid = Convert.ToInt32(locationid);
                int iyearid = Convert.ToInt32(yearid);

                DataSet dsPlanned = new DataSet();
                dsPlanned = _objdlweek.GetPlannedActivities_Service(icompanyid, ilocationid, iyearid);

                List<Model.PlannedActivity> lstCfs = new List<Model.PlannedActivity>();
                if (dsPlanned.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dsPlanned.Tables[0].Rows)
                    {
                        Model.PlannedActivity objPlannedActivity = new Model.PlannedActivity();
                        objPlannedActivity.Week = Convert.ToInt32(drPlanned["WeekID"].ToString());
                        objPlannedActivity.Planned = Convert.ToInt32(drPlanned["Planned"].ToString());
                        objPlannedActivity.Wdate = FirstDateOfWeek(iyearid, Convert.ToInt32(drPlanned["WeekID"].ToString())).AddDays(6).ToShortDateString();
                        objPlannedActivity.IsCurrentMonth =
                            FindCurrentMonth(Convert.ToInt32(drPlanned["WeekID"].ToString()));

                        objPlannedActivity.IsPending = CheckPending(Convert.ToInt32(drPlanned["WeekID"].ToString()), Convert.ToInt32(drPlanned["Planned"].ToString()), Convert.ToInt32(drPlanned["Updated"].ToString()));
                        objPlannedActivity.IsClosed = CheckClosed(Convert.ToInt32(drPlanned["Planned"].ToString()), Convert.ToInt32(drPlanned["Updated"].ToString()));
                        objPlannedActivity.Closed = Convert.ToInt32(drPlanned["Updated"].ToString());
                        lstCfs.Add(objPlannedActivity);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstCfs;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        private bool CheckPending(int weekno, int planned, int updated)
        {
            try
            {
                DateTime now = DateTime.Now;
                int currentweekno = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                if (weekno >= currentweekno)
                {
                    return false;
                }
                else
                {
                    if (planned - updated == 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
            catch
            {
                throw;
            }
        }

        private bool CheckClosed(int planned, int updated)
        {
            try
            {
                if (planned == 0)
                {
                    return false;
                }
                if (planned - updated == 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "Get Planned Activities - StatusWise"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetPlannedActivities_Statusewise(string companyid, string locationid, string yearid, string weekid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(companyid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(locationid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }


                if (string.IsNullOrWhiteSpace(yearid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Year", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(weekid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Week", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                // yearid = "2017"; //need to change when confirmation.

                int icompanyid = Convert.ToInt32(companyid);
                int ilocationid = Convert.ToInt32(locationid);
                int iyearid = Convert.ToInt32(yearid);
                int iweekid = Convert.ToInt32(weekid);

                DataSet dsPlanned = new DataSet();
                dsPlanned = _objdlweek.GetPlannedActivities_Statuswise(icompanyid, ilocationid, iyearid, iweekid);

                List<Model.StatusPlannedActivity> lstCfs = new List<Model.StatusPlannedActivity>();
                if (dsPlanned.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dsPlanned.Tables[0].Rows)
                    {
                        Model.StatusPlannedActivity objPlannedActivity = new Model.StatusPlannedActivity();
                        objPlannedActivity.StatusId = Convert.ToInt32(drPlanned["AssetWorkStatusID"].ToString());
                        objPlannedActivity.Count = Convert.ToInt32(drPlanned["Count"].ToString());
                        objPlannedActivity.Status = drPlanned["StatusName"].ToString();
                        lstCfs.Add(objPlannedActivity);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstCfs;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion

        #region "Get Permitted Company"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetPermittedCompany(string userid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(userid))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid User", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(userid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid User", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int iUserId = Convert.ToInt32(userid);

                DataSet ds = new DataSet();
                //ds = objRoleComp.GetRoleCompany(UserSession.RoleID, UserSession.CompanyIDUser);
                ds = objuserCompany.GetUserCompany(iUserId);

                DataTable dFiltered = ds.Tables[0].Clone();
                DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                foreach (DataRow dr in dRowUpdates)
                    dFiltered.ImportRow(dr);

                List<Model.Company> lstCfs = new List<Model.Company>();
                if (dFiltered.Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dFiltered.Rows)
                    {
                        Model.Company objPlannedActivity = new Model.Company();
                        objPlannedActivity.CompanyId = Convert.ToInt32(drPlanned["CompanyID"].ToString());
                        objPlannedActivity.CompanyName = drPlanned["CompanyName"].ToString();
                        lstCfs.Add(objPlannedActivity);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstCfs;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            return _res;
        }

        #endregion

        #region "Get Permitted Location"
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetPermittedLocation(string userid, string companyid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(userid))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid User", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(userid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid User", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(companyid))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(companyid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int iUserId = Convert.ToInt32(userid);
                int iCompanyId = Convert.ToInt32(companyid);

                DataSet ds = new DataSet();
                //ds = objRoleComp.GetRoleCompany(UserSession.RoleID, UserSession.CompanyIDUser);
                ds = objuserLocation1.GetUserLocation(iUserId, iCompanyId);

                DataTable dFiltered = ds.Tables[0].Clone();
                DataRow[] dRowUpdates = ds.Tables[0].Select(" UserLocationID > 0", "locationname");
                foreach (DataRow dr in dRowUpdates)
                    dFiltered.ImportRow(dr);

                List<Model.Location> lstCfs = new List<Model.Location>();
                if (dFiltered.Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dFiltered.Rows)
                    {
                        Model.Location objPlannedActivity = new Model.Location();
                        objPlannedActivity.LocationId = Convert.ToInt32(drPlanned["locationid"].ToString());
                        objPlannedActivity.LocationName = drPlanned["locationname"].ToString();
                        lstCfs.Add(objPlannedActivity);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstCfs;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            return _res;
        }

        #endregion

        #region "Get Planned Activities - StatusWise - week"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetPlannedActivities_StatusWeekwise(string companyid, string locationid, string yearid, string weekid, string statusid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(companyid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(locationid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }


                if (string.IsNullOrWhiteSpace(yearid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Year", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(weekid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Week", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(statusid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Staus", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(statusid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Staus", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                // yearid = "2017"; //need to change when confirmation.

                int icompanyid = Convert.ToInt32(companyid);
                int ilocationid = Convert.ToInt32(locationid);
                int iyearid = Convert.ToInt32(yearid);
                int iweekid = Convert.ToInt32(weekid);
                int iStatusId = Convert.ToInt32(statusid);

                DataSet dsPlanned = new DataSet();
                dsPlanned = _objdlweek.GetUpdatedDetailsByWeek(icompanyid, ilocationid, iyearid, iweekid, iStatusId);

                List<Model.UpdatedDetails> lstplanned = new List<Model.UpdatedDetails>();
                if (dsPlanned.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dsPlanned.Tables[0].Rows)
                    {
                        Model.UpdatedDetails objPlannedActivity = new Model.UpdatedDetails();
                        objPlannedActivity.StatusId = Convert.ToInt32(drPlanned["Statusid"].ToString());
                        objPlannedActivity.AssetId = Convert.ToInt32(drPlanned["AssetID"].ToString());
                        objPlannedActivity.WeekId = Convert.ToInt32(drPlanned["WeekId"].ToString());
                        objPlannedActivity.DepartmentId = Convert.ToInt32(drPlanned["DepartmentID"].ToString());
                        objPlannedActivity.Status = drPlanned["Status"].ToString();
                        objPlannedActivity.AssetName = drPlanned["AssetName"].ToString();
                        objPlannedActivity.PlannerId = drPlanned["PlannerName"].ToString();
                        objPlannedActivity.IsClosed = Convert.ToBoolean(drPlanned["IsClosed"].ToString());
                        objPlannedActivity.AssetAssignId = Convert.ToInt32(drPlanned["AssetAssignID"].ToString());
                        objPlannedActivity.Frequency = drPlanned["FrequencyName"].ToString();

                        objPlannedActivity.Building = drPlanned["Building"].ToString();
                        objPlannedActivity.BuildingShortName = drPlanned["BuildingShortName"].ToString();

                        objPlannedActivity.Floor = drPlanned["floor"].ToString();
                        objPlannedActivity.FloorShortName = drPlanned["FloorShortName"].ToString();

                        objPlannedActivity.FDate = drPlanned["FDate"].ToString();
                        objPlannedActivity.FTime = drPlanned["FTime"].ToString();

                        lstplanned.Add(objPlannedActivity);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstplanned;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion

        #region "Get Planned Activities"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetStatusDetails(string companyid, string locationid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(companyid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(locationid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int icompanyid = Convert.ToInt32(companyid);
                int ilocationid = Convert.ToInt32(locationid);


                DataSet dsStatus = new DataSet();
                dsStatus = _objdlweek.GetStatusDetails(icompanyid, ilocationid);

                List<Model.Status> lstStatus = new List<Model.Status>();
                if (dsStatus.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dsStatus.Tables[0].Rows)
                    {
                        Model.Status objStatusDet = new Model.Status();
                        objStatusDet.StatusId = Convert.ToInt32(drPlanned["StatusID"].ToString());
                        objStatusDet.StatusName = drPlanned["StatusName"].ToString();
                        objStatusDet.isclosed = Convert.ToBoolean(drPlanned["isclosed"].ToString());
                        lstStatus.Add(objStatusDet);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstStatus;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion

        #region "Get Planned Activities"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetAssignedAssetDetails(string assetAssignedId)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(assetAssignedId.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Assigned ID", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int iassetAssignedId = Convert.ToInt32(assetAssignedId);


                DataSet dsAssignedSet = new DataSet();
                dsAssignedSet = _objdlweek.GetAssignedDetails_Mobile(iassetAssignedId);

                List<Model.AssignedDet> lstStatus = new List<Model.AssignedDet>();
                if (dsAssignedSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dsAssignedSet.Tables[0].Rows)
                    {
                        Model.AssignedDet objStatusDet = new Model.AssignedDet();
                        objStatusDet.AssetName = drPlanned["AssetName"].ToString();
                        objStatusDet.PlannerName = drPlanned["PlannerName"].ToString();
                        objStatusDet.StatusId = Convert.ToInt32(drPlanned["StatusId"].ToString());
                        objStatusDet.FrequencyDate = drPlanned["FrequencyDate"].ToString();
                        objStatusDet.WeekId = Convert.ToInt32(drPlanned["WeekID"].ToString());
                        objStatusDet.AssetAssignedId = Convert.ToInt32(drPlanned["AssetAssignID"].ToString());
                        objStatusDet.Status = drPlanned["Status"].ToString();
                        objStatusDet.IsClosed = Convert.ToBoolean(drPlanned["IsClosed"].ToString());
                        objStatusDet.IsOverDue = Convert.ToBoolean(drPlanned["IsOverDue"].ToString());
                        objStatusDet.Remarks = drPlanned["Remarks"].ToString();
                        objStatusDet.checklists = GetChecklists(drPlanned["Checklists"].ToString());
                        lstStatus.Add(objStatusDet);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstStatus;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion

        private void Checkno()
        {
            int luckyno = 7;
            string permutation = string.Empty;
            for (int icount = 1; icount <= 9999; icount++)
            {
                int no = icount;
                int val = 0;
                while (no > 0)
                {
                    int rem = no % 10;
                    val = val + rem;
                    no = no / 10;
                }
                if (val == luckyno)
                {
                    permutation = permutation + icount.ToString() + ",";
                }
            }
        }

        #region "Asset Update"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string AssetUpdate(string assetassignId, string statusId, string remarks, string isOverdue,
            string weekdayid, string userid, string updateddate, string checklistxml, string files)
        {
            string _res = string.Empty;
            try
            {
                //Checkno();
                if (string.IsNullOrWhiteSpace(assetassignId.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid AssetAssignedId", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(statusId.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid StatusId", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(isOverdue.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid isOverdue", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(weekdayid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid weekdayid", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(userid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid userid", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(updateddate.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid updateddate", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                IFormatProvider culture = new CultureInfo("en-US", true);

                DateTime? dtUpdatedTime;
                if (updateddate != string.Empty)
                {
                    dtUpdatedTime = DateTime.ParseExact(updateddate, "d/M/yyyy HH:mm:ss", culture);
                }
                else
                {
                    dtUpdatedTime = null;
                }

                int iAssetAssignId = Convert.ToInt32(assetassignId);
                int iStatusId = Convert.ToInt32(statusId);
                bool bisOverdue = Convert.ToBoolean(isOverdue);
                int iweekdayid = Convert.ToInt32(weekdayid);
                int iuserid = Convert.ToInt32(userid);

                string filenames = string.Empty;
                if (files != string.Empty)
                {
                    string[] multiplefiles = files.Split('#');
                    foreach (string bas64Image in multiplefiles)
                    {
                        if (bas64Image.Trim() != string.Empty)
                        {
                            //string fileguid = Guid.NewGuid().ToString();
                            //byte[] filebytearray = Convert.FromBase64String(bas64Image);
                            //ByteArrayToImage(filebytearray, fileguid);
                            //fileguid = fileguid + ".png";
                            //filenames = filenames + fileguid + ",";

                            string fileguid = Guid.NewGuid().ToString() + ".png";

                            string _awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
                            string _awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
                            string _bucketName = ConfigurationManager.AppSettings["Bucketname"];
                            //string filename = Guid.NewGuid().ToString() + ".png";

                            Amazon.S3.AmazonS3 client;
                            byte[] bytes = Convert.FromBase64String(bas64Image);

                            if (bytes.Length > 0)
                            {
                                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(_awsAccessKey, _awsSecretKey))
                                {
                                    string currentyear = DateTime.UtcNow.Year.ToString();
                                    var request = new PutObjectRequest
                                    {
                                        BucketName = _bucketName,
                                        CannedACL = S3CannedACL.PublicRead,
                                        Key = string.Format("uploadimages/" + currentyear + "/{0}", fileguid)
                                    };
                                    using (var ms = new MemoryStream(bytes))
                                    {
                                        request.InputStream = ms;
                                        S3Response response = client.PutObject(request);
                                        filenames = filenames + fileguid + ",";
                                    }
                                }
                            }

                        }
                    }
                    filenames = filenames.TrimEnd(',');
                }



                string[] values = _objdlweek.AssetUpdate(iAssetAssignId, iStatusId, remarks, bisOverdue, iweekdayid, iuserid, dtUpdatedTime, checklistxml, filenames);

                if (values[1] != "-1")
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = values[0], Status = true }, Newtonsoft.Json.Formatting.Indented);
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = values[0], Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                return _res;
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);

                AppendLog(assetassignId.ToString() + "-" + ex.ToString());
            }
            return _res;
        }

        public static void AppendLog(string Log)
        {
            string filename = DateTime.Now.ToString("yyyyMMdd");
            StreamWriter sw = File.AppendText(System.AppDomain.CurrentDomain.BaseDirectory + "Log/" + filename + "Log.log");
            sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " :: " + Log);
            sw.Close();
            sw.Dispose();
        }

        //private string RemoveSpecailCharacters(string xmlstring)
        //{
        //    try
        //    {
        //        xmlstring = xmlstring.Replace("<", "&lt;");
        //        xmlstring = xmlstring.Replace("&", "");
        //        xmlstring = xmlstring.Replace(">", "");
        //        xmlstring = xmlstring.Replace(""", "");
        //        xmlstring = xmlstring.Replace("'", "");
        //        return xmlstring;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}


        private void ByteArrayToImage(byte[] byteArrayIn, string filename)
        {
            try
            {
                string Path = ConfigurationManager.AppSettings["filepath"].ToString() + filename;
                System.Drawing.Image newImage;
                if (byteArrayIn != null)
                {
                    using (MemoryStream stream = new MemoryStream(byteArrayIn))
                    {
                        newImage = System.Drawing.Image.FromStream(stream);
                        newImage.Save(Path + "." + System.Drawing.Imaging.ImageFormat.Png);
                        newImage.Dispose();
                        newImage = null;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        #endregion


        #region Additional Functions

        public static DateTime FirstDateOfWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        public bool FindCurrentMonth(int week)
        {
            DateTime now = DateTime.Now;
            //var startDate = new DateTime(now.Year, now.Month, 1);
            //var endDate = startDate.AddMonths(1).AddDays(-1);
            //return dtAssignDate >= startDate && dtAssignDate < endDate;
            int currentweekno = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            if (week == currentweekno)
            {
                return true;
            }
            return false;
        }

        #endregion


        #region "Get Department Details"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetDepartments(string companyid, string locationid)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(companyid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(locationid.ToString()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int icompanyid = Convert.ToInt32(companyid);
                int ilocationid = Convert.ToInt32(locationid);


                DataSet dsdept = new DataSet();
                dsdept = _objdlweek.GetDepartmentDetails(icompanyid, ilocationid);

                List<Model.Department> lstdept = new List<Model.Department>();
                if (dsdept.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drPlanned in dsdept.Tables[0].Rows)
                    {
                        Model.Department objdept = new Model.Department();
                        objdept.DepartmentId = Convert.ToInt32(drPlanned["DepartmentID"].ToString());
                        objdept.DepartmentName = drPlanned["DepartmentName"].ToString();
                        lstdept.Add(objdept);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstdept;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion


        #region "Get Scanned Activities"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetScannedAssetDetails(string AssetId, string FrequencyDate, string WeekNo)
        {
            string _res = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(AssetId.ToString().Trim()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Assigned ID", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(FrequencyDate.ToString().Trim()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Freqeuncy Date", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(WeekNo.ToString().Trim()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Week No", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int iassetId = Convert.ToInt32(AssetId);
                int iweeno = Convert.ToInt16(WeekNo);
                IFormatProvider culture = new CultureInfo("en-US", true);

                DateTime dtUpdatedTime;
                if (FrequencyDate != string.Empty)
                {
                    dtUpdatedTime = DateTime.ParseExact(FrequencyDate, "M/d/yyyy", culture);
                }
                else
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Frequency Date", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                DataSet dsAssignedSet = new DataSet();
                dsAssignedSet = _objdlweek.GetAssetAssignAndOverDueDetials(iassetId, dtUpdatedTime.Year, iweeno);

                if (dsAssignedSet != null && dsAssignedSet.Tables.Count > 0 && dsAssignedSet.Tables[0].Rows.Count > 0)
                {
                    string results1 = ConvertJavaSeriptSerializer(dsAssignedSet.Tables[0]);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetScannedAssetDetailsNew(string AssetId, string FrequencyDate, string WeekNo)
        {
            string _res = string.Empty;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (string.IsNullOrWhiteSpace(AssetId.ToString().Trim()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Assigned ID", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(FrequencyDate.ToString().Trim()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Freqeuncy Date", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                if (string.IsNullOrWhiteSpace(WeekNo.ToString().Trim()))
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Week No", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                int iassetId = Convert.ToInt32(AssetId);
                int iweeno = Convert.ToInt16(WeekNo);
                IFormatProvider culture = new CultureInfo("en-US", true);

                DateTime dtUpdatedTime;
                if (FrequencyDate != string.Empty)
                {
                    dtUpdatedTime = DateTime.ParseExact(FrequencyDate, "M/d/yyyy", culture);
                }
                else
                {
                    return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Frequency Date", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }

                DataSet dsAssignedSet = new DataSet();
                dsAssignedSet = _objdlweek.GetAssetAssignAndOverDueDetials(iassetId, dtUpdatedTime.Year, iweeno);

                if (dsAssignedSet != null && dsAssignedSet.Tables.Count > 0 && dsAssignedSet.Tables[0].Rows.Count > 0)
                {
                    List<ScannedDetails> objscned = new List<ScannedDetails>();
                    foreach (DataRow drassign in dsAssignedSet.Tables[0].Rows)
                    {
                        ScannedDetails objcklst = new ScannedDetails();
                        objcklst.CompanyID = Convert.ToInt32(drassign["companyid"].ToString());
                        objcklst.LocationID = Convert.ToInt32(drassign["Locationid"].ToString());
                        objcklst.WeekID = Convert.ToInt32(drassign["WeekID"].ToString());
                        objcklst.AssetAssignID = Convert.ToInt32(drassign["AssetAssignID"].ToString());
                        objcklst.PlannerName = Convert.ToString(drassign["PlannerName"].ToString());
                        objcklst.AssetName = Convert.ToString(drassign["AssetName"].ToString());
                        objcklst.FrequencyDate = Convert.ToString(drassign["FrequencyDate"].ToString());
                        objcklst.StatusId = Convert.ToInt32(drassign["StatusId"].ToString());
                        objcklst.Status = Convert.ToString(drassign["Status"].ToString());
                        objcklst.IsClosed = Convert.ToBoolean(drassign["IsClosed"].ToString());
                        objcklst.IsOverDue = Convert.ToBoolean(drassign["IsOverDue"].ToString());
                        objcklst.FrequencyName = Convert.ToString(drassign["Frequency"].ToString());
                        objcklst.FrequencyID = Convert.ToInt32(drassign["FrequencyID"].ToString());
                        objcklst.checklists = GetChecklists(drassign["Checklists"].ToString());
                        objscned.Add(objcklst);
                    }

                    object resultMain = new object();
                    resultMain = objscned;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }


        public class Checklist
        {
            public string checklist { get; set; }
        }

        public class ScannedDetails
        {
            public int CompanyID { get; set; }
            public int LocationID { get; set; }
            public int WeekID { get; set; }
            public int AssetAssignID { get; set; }
            public string PlannerName { get; set; }
            public string AssetName { get; set; }
            public string FrequencyDate { get; set; }
            public int StatusId { get; set; }
            public string Status { get; set; }
            public bool IsClosed { get; set; }
            public bool IsOverDue { get; set; }
            public string FrequencyName { get; set; }
            public int FrequencyID { get; set; }
            public List<Model.Checklist> checklists { get; set; }
        }

        private List<Model.Checklist> GetChecklists(string checklists)
        {
            try
            {
                string[] splchklists = checklists.Split(new string[] { "`!" }, StringSplitOptions.None);
                List<Model.Checklist> objcklst = new List<Model.Checklist>();
                foreach (string chklst in splchklists)
                {
                    int slno = 1;
                    if (chklst.Trim() != string.Empty)
                    {
                        Model.Checklist objchk = new Model.Checklist();
                        string[] splids = chklst.Split(new string[] { "$%" }, StringSplitOptions.None);
                        if (splids.Length > 0)
                        {
                            objchk.checklistid = Convert.ToInt32(splids[0]);
                            objchk.checklist = splids[1];
                            objchk.IsChecked = splids[2] == "1" ? true : false;
                            objchk.Remarks = splids[3];
                            objchk.Slno = slno;
                            objcklst.Add(objchk);
                            slno++;
                        }
                    }
                }
                return objcklst;
            }
            catch
            {
                throw;
            }
        }

        #endregion




        private string ConvertJavaSeriptSerializer(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            List<object> resultMain = new List<object>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    result.Add(column.ColumnName, "" + row[column.ColumnName]);
                }
                resultMain.Add(result);
            }
            return serializer.Serialize(resultMain);
        }

    }
}
